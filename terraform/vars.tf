variable "ami" {
  type    = string
  default = "ami-0d058fe428540cd89"
}


variable "instance_type" {
  type    = string
  default = "t2.micro"
}


variable "key_name" {
    type = string
    default = “cloud-academy"
}
