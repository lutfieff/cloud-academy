terraform {
  backend "s3" {
    bucket = "ca-tfstate-username"
    key = "tfstate"
    region = "ap-southeast-1"
  }
}
