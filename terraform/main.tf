resource "aws_security_group" "sshhttp" {
    name = "Allow_CA-Username"
    ingress {
        description = "Public SSH"
        from_port = 22
        to_port = 22
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
        }
     ingress {
        description = "Public SSH"
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
        }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "default" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = var.key_name
  user_data     = <<-EOF
                  #!/bin/bash
                  sudo apt-get update
                  sudo apt install git-all
                  EOF
  vpc_security_group_ids = [aws_security_group.sshhttp.id]
  tags = {
      Name = “ca-tf-Username"
  }
}
